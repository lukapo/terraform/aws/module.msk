Provider Requirements:
* **aws:** (any version)

## Input Variables
* `kafka_broker_number` (default `1`): Kafka brokers per zone
* `kafka_custom_config` (default `"{}"`): Kafka custom config json file
* `kafka_ebs_volume_size` (default `"100"`): Kafka EBS volume size in GB
* `kafka_encryption_in_transit` (default `"TLS_PLAINTEXT"`): Encryption setting for data in transit between clients and brokers. Valid values: TLS, TLS_PLAINTEXT, and PLAINTEXT.
* `kafka_instance_type` (default `"kafka.m5.large"`): Kafka broker instance type
* `kafka_monitoring_level` (default `"PER_TOPIC_PER_BROKER"`): property to one of three monitoring levels: DEFAULT, PER_BROKER, or PER_TOPIC_PER_BROKER
* `kafka_version` (default `"2.2.1"`): Version of Kafka brokers
* `name_prefix` (required): A prefix used for naming resources.
* `subnet_numbers` (default `{"eu-central-1a":1,"eu-central-1b":2,"eu-central-1c":3}`): Map from availability zone to the number that should be used for each availability zone's subnet
* `tags` (required): A map of tags (key-value pairs) passed to resources
* `vpc_cidr_block` (required): VPC cidr block
* `vpc_id` (required): VPC ID

## Output Values
* `bootstrap_brokers`: Plaintext connection host:port pairs
* `bootstrap_brokers_tls`: TLS connection host:port pairs
* `zookeeper_connect_string`

## Managed Resources
* `aws_kms_key.kms` from `aws`
* `aws_msk_cluster.msk-cluster` from `aws`
* `aws_msk_configuration.mks-cluster-custom-configuration` from `aws`
* `aws_security_group.sg` from `aws`

## Data Resources
* `data.aws_availability_zones.azs` from `aws`

