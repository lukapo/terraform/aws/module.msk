variable "kafka_custom_config_auto_create_topics" {
  description = "Enables topic autocreation on the server"
  type        = bool
  default     = true
}

variable "kafka_custom_config_delete_topics" {
  description = "Enables the delete topic operation. If this config is turned off, you can't delete a topic through the admin tool"
  type        = bool
  default     = true
}

variable "kafka_custom_config_log_retention" {
  description = "Number of hours to keep a log file before deleting it, tertiary to the log.retention.ms property."
  type        = number
  default     = 720
}

variable "kafka_custom_config_num_partitions" {
  description = "Default number of log partitions per topic."
  type        = number
  default     = 10
}

variable "subnet_numbers" {
  description = "Map from availability zone to the number that should be used for each availability zone's subnet"
  type        = map(string)
  default = {
    "eu-central-1a" = 1
    "eu-central-1b" = 2
    "eu-central-1c" = 3
  }
}
