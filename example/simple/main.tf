provider "aws" {
  region = "eu-central-1"
}

data "aws_vpc" "main" {
  default = true
}

locals {
  subnets     = ["192.100.0.0/24", "192.100.10.0/24", "192.100.20.0/24"]
  name_prefix = "test"
}

resource "aws_subnet" "subnet" {
  for_each = var.subnet_numbers

  vpc_id            = data.aws_vpc.main.id
  availability_zone = each.key
  cidr_block        = cidrsubnet(local.subnets[each.value - 1], 0, 0)
}

data "template_file" "msk-custom-configuration" {
  template = file("msk-custom-configuration.json.tpl")
  vars = {
    auto_create_topics_enable = var.kafka_custom_config_auto_create_topics
    delete_topic_enable       = var.kafka_custom_config_delete_topics
    log_retention_hours       = var.kafka_custom_config_log_retention
    num_partitions            = var.kafka_custom_config_num_partitions
  }
}

module "msk-cluster" {
  source = "../../"

  name_prefix         = local.name_prefix
  vpc_id              = data.aws_vpc.main.id
  vpc_cidr_block      = concat([aws_subnet.subnet["eu-central-1a"].cidr_block], [aws_subnet.subnet["eu-central-1b"].cidr_block], [aws_subnet.subnet["eu-central-1c"].cidr_block])
  kafka_custom_config = data.template_file.msk-custom-configuration.rendered

}