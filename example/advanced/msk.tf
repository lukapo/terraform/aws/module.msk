module "msk" {
  source = "./msk"

  vpc_id        = module.vpc.vpc_id
  subnet_list   = module.vpc.database_subnets
  name_prefix   = "msk-test"
  kafka_version = "2.2.1"
}
