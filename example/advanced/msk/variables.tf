variable "kafka_custom_config_auto_create_topics" {
  description = "Enables topic autocreation on the server"
  type        = bool
  default     = true
}

variable "kafka_custom_config_delete_topics" {
  description = "Enables the delete topic operation. If this config is turned off, you can't delete a topic through the admin tool"
  type        = bool
  default     = true
}

variable "kafka_custom_config_log_retention" {
  description = "Number of hours to keep a log file before deleting it, tertiary to the log.retention.ms property."
  type        = number
  default     = 720
}

variable "kafka_custom_config_num_partitions" {
  description = "Default number of log partitions per topic."
  type        = number
  default     = 10
}

variable "vpc_id" {
  description = "ID of the VPC to deploy database into"
  type        = string
}

variable "name_prefix" {
  description = "Prefix for kafka cluster name"
  type        = string
}

variable "subnet_list" {
  description = "A list of subnets"
  default     = []
  type        = list(string)
}

variable "kafka_version" {
  description = "A version of Kafka brokers"
  default     = "2.2.1"
  type        = string
}
