data "template_file" "msk-custom-configuration" {
  template = file("./msk/msk-custom-configuration.json.tpl")
  vars = {
    auto_create_topics_enable = var.kafka_custom_config_auto_create_topics
    delete_topic_enable       = var.kafka_custom_config_delete_topics
    log_retention_hours       = var.kafka_custom_config_log_retention
    num_partitions            = var.kafka_custom_config_num_partitions
  }
}


module "msk-cluster" {
  source = "../../../"

  name_prefix         = var.name_prefix
  vpc_id              = var.vpc_id
  vpc_cidr_block      = var.subnet_list
  kafka_version       = var.kafka_version
  kafka_custom_config = data.template_file.msk-custom-configuration.rendered

}
