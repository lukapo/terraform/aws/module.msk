module "vpc" {
  source = "./templates/vpc"

  aws_region = local.aws_region

  # vpc

  vpc_name = "vpc-${local.env_project}"
  cidr     = local.vpc_cidr

  azs                          = ["${local.aws_region}a", "${local.aws_region}b", "${local.aws_region}c"]
  private_subnets              = ["${local.vpc_cidr_prefix}.5.0/24", "${local.vpc_cidr_prefix}.15.0/24", "${local.vpc_cidr_prefix}.25.0/24", "${local.vpc_cidr_prefix}.35.0/24"]
  public_subnets               = ["${local.vpc_cidr_prefix}.0.0/24", "${local.vpc_cidr_prefix}.10.0/24", "${local.vpc_cidr_prefix}.20.0/24", "${local.vpc_cidr_prefix}.30.0/24"]
  database_subnets             = ["${local.vpc_cidr_prefix}.100.0/24", "${local.vpc_cidr_prefix}.101.0/24", "${local.vpc_cidr_prefix}.102.0/24"]
  create_database_subnet_group = false
  enable_dns_support           = true
  enable_dns_hostnames         = true
  enable_nat_gateway           = true
  single_nat_gateway           = false

  tags = {
    Project     = local.project
    Environment = local.environment
  }
}
