locals {
  aws_region = "eu-central-1"

  project     = "test"
  environment = "dev"
  env_project = "${local.project}-${local.environment}"

  ## VPC ##

  vpc_cidr        = "10.10.0.0/16"
  vpc_cidr_prefix = "10.10"

  ## MSK ##

  kafka_version = "2.2.1"
}
