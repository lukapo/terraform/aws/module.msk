Provider Requirements:
* **aws:** (any version)
* **template:** (any version)

## Input Variables
* `kafka_custom_config_auto_create_topics` (default `true`): Enables topic autocreation on the server
* `kafka_custom_config_delete_topics` (default `true`): Enables the delete topic operation. If this config is turned off, you can't delete a topic through the admin tool
* `kafka_custom_config_log_retention` (default `720`): Number of hours to keep a log file before deleting it, tertiary to the log.retention.ms property.
* `kafka_custom_config_num_partitions` (default `10`): Default number of log partitions per topic.
* `subnet_numbers` (default `{"eu-central-1a":1,"eu-central-1b":2,"eu-central-1c":3}`): Map from availability zone to the number that should be used for each availability zone's subnet

## Managed Resources
* `aws_subnet.subnet` from `aws`

## Data Resources
* `data.aws_vpc.main` from `aws`
* `data.template_file.msk-custom-configuration` from `template`

## Child Modules
* `msk-cluster` from `../../`

tuma@korneli:~/Work/module.msk-cluster$ /home/tuma/go/bin/terraform-config-inspect /home/tuma/Work/module.msk-cluster/example/advanced/

# Module `/home/tuma/Work/module.msk-cluster/example/advanced/`

Provider Requirements:
* **aws:** (any version)

## Child Modules
* `msk` from `./msk`
* `vpc` from `./templates/vpc`
