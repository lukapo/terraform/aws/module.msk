resource "aws_security_group" "sg" {
  name        = "security_group-${var.name_prefix}-msk"
  description = "MSK security group (only 9092, 9094 and  2181 inbound access is allowed)"
  vpc_id      = var.vpc_id

  tags = {
    Name = "sg-${var.name_prefix}-msk"
  }
}

resource "aws_security_group_rule" "tls_ingress" {
  type              = "ingress"
  from_port         = "9094"
  to_port           = "9094"
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr
  security_group_id = aws_security_group.sg.id
}

resource "aws_security_group_rule" "plain_ingress" {
  type              = "ingress"
  from_port         = "9092"
  to_port           = "9092"
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr
  security_group_id = aws_security_group.sg.id
}

resource "aws_security_group_rule" "zk_ingress" {
  type              = "ingress"
  from_port         = "2181"
  to_port           = "2181"
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr
  security_group_id = aws_security_group.sg.id
}

resource "aws_security_group_rule" "msk_all_egress" {
  type      = "egress"
  from_port = "0"
  to_port   = "65535"
  protocol  = "all"

  cidr_blocks = [
    "0.0.0.0/0",
  ]

  ipv6_cidr_blocks = [
    "::/0",
  ]

  security_group_id = aws_security_group.sg.id
}

data "aws_availability_zones" "azs" {
  state = "available"
}

resource "aws_kms_key" "kms" {
  description = "${var.name_prefix}-kms_key"
}

resource "aws_msk_cluster" "msk-cluster" {
  cluster_name           = "${var.name_prefix}-msk-cluster"
  kafka_version          = var.kafka_version
  number_of_broker_nodes = var.kafka_broker_number

  broker_node_group_info {
    instance_type   = var.kafka_instance_type
    ebs_volume_size = var.kafka_ebs_volume_size
    client_subnets  = var.vpc_cidr_block
    security_groups = [aws_security_group.sg.id]
  }

  encryption_info {
    encryption_in_transit {
      client_broker = var.kafka_encryption_in_transit
    }
    encryption_at_rest_kms_key_arn = aws_kms_key.kms.arn
  }

  configuration_info {
    arn      = "${aws_msk_configuration.mks-cluster-custom-configuration.arn}"
    revision = "${aws_msk_configuration.mks-cluster-custom-configuration.latest_revision}"
  }

  enhanced_monitoring = var.kafka_monitoring_level

  tags = var.tags
}

resource "aws_msk_configuration" "mks-cluster-custom-configuration" {
  kafka_versions = [var.kafka_version]
  name           = "${var.name_prefix}-mks-cluster-custom-configuration"

  server_properties = var.kafka_custom_config
}
